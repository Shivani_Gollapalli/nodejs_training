//Importing Modules
const express = require("express");
const mongoose=require("mongoose")
const fs=require("fs")
let App = express()

//Connecting To MongoDB String
mongoose.connect("mongodb+srv://eSignsdbuser:e5!G%5E5TGWH1bKBRv9gCtn5@cluster3.v7pzg8d.mongodb.net/Shivani")

//MiddleWare
App.use(express.json())

//Creating Schema
const userSchema=mongoose.Schema(
    {
        CompanyId:{type:String,require:true},
        FirstName:{type:String,required:true},
        LastName:{type:String,required:true},
        DomainName:{type:String,required:true},
        PhoneNumber:{type:String,unique:true,required:true},
        Email:{type:String,unique:true,required:true},
        Password:{type:String,unique:true,required:true}
    }
    
)

//Creating Model
const User=mongoose.model("user_info",userSchema)

//GET METHOD
App.get('/allusers',async (req,res)=>{
    try{
        let results=await User.find();
        res.status(200).json({
            data:results
        })
    }
    catch(error){
        console.log(error)
    }
})

//POST METHOD
App.post('/create',async (req,res)=>{
    let data=new User(req.body)
    const result=await data.save()
    res.send(result)
})

//PATCH METHOD 

App.patch('/:CompanyId', async (req, res) => {
    const CompanyId = req.params.CompanyId;

    try {
        const result = await User.findByIdAndUpdate(CompanyId, req.body, { new: true });
        if (!result) {
            return res.status(404).json({ error: 'User not found' });
        }

        res.status(200).json({ data: result });
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});


//Creating Server
const port=3000;
App.listen(port,()=>{
    console.log("Server is Running...!!")
})

// Get-ExecutionPolicy
// Set-ExecutionPolicy RemoteSigned -Scope Process
// npm install -g nodemon --save 
//nodemon FileName