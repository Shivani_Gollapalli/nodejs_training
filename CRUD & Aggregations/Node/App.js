//Importing Required Modules
const express=require("express") 
const cors=require('cors')
const studentRouter=require('./Routers.js')
const authRouter=require('./AuthRouter.js')

//Express Instance
let App=express()
//Display Request Body
App.use(express.json())
App.use(cors());

// app.use(morgon('dev'))

//Routing API'S
App.use('/students',studentRouter)
App.use('/users',authRouter)

module.exports=App

// Get-ExecutionPolicy
// Set-ExecutionPolicy RemoteSigned -Scope Process
// npm install -g nodemon --save 
//nodemon FileName
