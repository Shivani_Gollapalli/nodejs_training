//Importing Required Modules
const User=require('./UserModel.js')
const jwt=require('jsonwebtoken')
const util=require('util')

//Verifying SignToken or Not
const signToken=id=>{
    return jwt.sign({id},process.env.SECRET_STR,{
        expiresIn:process.env.LOGIN_EXP
    }
)}

//Signup Funtion
exports.signup = async(req,res,next)=>{
    try{
        const newUser=await User.create(req.body);
        const token=signToken(newUser._id);
        res.status(201).json({
            status:"Success",
            Token:token,
            data:{
                user:newUser
            }
        })
    }catch(err){
        res.status(404).json({
            status:"Fail",
            message:err.message
        })
    }
}

//Login Function
exports.login=async (req,res,next)=>{
    try{
        const email=await req.body.email;
        const password=await req.body.password;
        if (!email || !password) {
            return res.status(400).json({
                status: "Fail",
                message: "Please provide an email and password for login",
            });
        }

        //Check if user Exists with Given Email
        const user = await User.findOne({email}).select('+password');
        const isMatch = await user.comparePasswordDB(password,user.password);
        if(!user || !isMatch){
            return res.status(400).json({
                status: "Fail",
                message: "Incorrect Email or Password",
            });
        }
        const token=signToken(user._id)


        res.status(200).json({
            status:"Success",
            token,
            // user
        })
    }catch(err){
        res.status(404).json({
            status:"Fail",
            message:err.message
        })
    }
}

exports.protect=async(req,res,next)=>{
    try{
        //1.Read the Token & Check if it exist
        const testToken=req.headers.authorization
        let token;
        if(testToken && testToken.startsWith('Bearer')){
            token=testToken.split(' ')[1];
        }
        if(!token){
            return res.status(400).json({
                status: "Fail",
                message: "You are Not Logged in!"
            });
        }

        //2.valide the token
        const decodeToken=await util.promisify(jwt.verify)(token,process.env.SECRET_STR)
        console.log(decodeToken)

        //3.if the user exits
        const user=await User.findById(decodeToken.id);
        if(!user){
            return res.status(401).json({
                status: "Fail",
                message: "The User With the given token does not exist"
            });
        }

        //4.if the user changed password after the token was issued
        const isPasswordChanged=await user.isPasswordChanged(decodeToken.iat)
        if(isPasswordChanged){
            return res.status(401).json({
                status: "Fail",
                message: "The password has been changed recently . Please Login Again...!!!"
            });
        }

        //5.Allow user to access route
        req.user=user
        next();
        }catch(err){
            res.status(404).json({
                status:"Fail",
                message:err.message
            })
        }
    }

exports.restrict=(role)=>{
    return(req,res,next)=>{
        if(req.user.role!==role){
            return res.status(403).json({
                status: "Fail",
                message: "You Don't Have Any Access to Delete The Student Object"
            });
        }
        next();
    }
}

// exports.restrict=(...role)=>{
//     return(req,res,next)=>{
//         if(!role.includes(res.user.role)){
//             return res.status(403).json({
//                 status: "Fail",
//                 message: "You Don't Have Any Access to Delete The Student Object"
//             });
//         }
//         next();
//     }
// }