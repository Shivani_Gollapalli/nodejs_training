//Importing Required Modules
const mongoose=require('mongoose')  //For Mongo Connection
const dotenv=require("dotenv")      //For Environment Variables
dotenv.config({path:'./config.env'});
const App=require('./App.js')

// Conneting String
mongoose.connect(process.env.LOCAL_STR,{
    useNewUrlParser:true
}).then((conn)=>{
    // console.log(conn)
    console.log("DB Connected Successfully...")
}).catch((error)=>{
    console.log("Some error has occcured")
})

//Creating Server
const port=3000;
App.listen(port,()=>{
    console.log("Server is Running...!!!")
})