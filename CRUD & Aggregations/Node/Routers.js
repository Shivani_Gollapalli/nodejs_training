//Importing Required Modules
const express=require('express')            // for building both simple and complex web applications and APIs. 
const studentController=require('./Controller.js')
const AuthController=require('./AuthController.js')

//instantiation of an Express Router  mountable route handlers for your Express application
const Router=express.Router()            
// app.get('/AllStudents',getAllStudents)
// app.post('/create',createStudent)
// app.get('/:id',singleStudent)
// app.patch("/update/:id",updateStudent)
// app.delete("/delete/:id",deleteStudent)

Router.route('/create').post(studentController.createStudent)
Router.route('/All').get(AuthController.protect,studentController.getAllStudents)
Router.route('/std-stats').get(studentController.getStdStats)
Router.route('/std-by-address/:address').get(studentController.getStdByAddress)
// Router.route('/:id').get(studentController.singleid)
// Router.route('/update/:id').patch(studentController.updateStudent)
Router.route('/delete/:id').delete(AuthController.protect,AuthController.restrict("admin"),studentController.deleteStudent)

//Exportng Module
module.exports=Router;