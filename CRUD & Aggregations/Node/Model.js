//Importing Required Modules
const mongoose=require("mongoose")      //To Store the data in DB

//Craating Scehema with Specific Fields
const STD_INFO = new mongoose.Schema({
    full_name:{
        type:String
    },
    id:{
        type:String
    },
    branch:{
        type:String    
    },
    join_year:{
        type:Number
    },
    current_year:{
        type:Number
    },
    age:{
        type:Number
    },
    attendence:{
        type:Number
    },
    address:{
        type:[String]
    },
    subject_marks:{
        type:[Number]
    },
    cgpa:{
        type:Number
    }
})

//Creating Model
const StdDetails = mongoose.model("StudentDetails",STD_INFO)

//Exporting Module
module.exports=StdDetails