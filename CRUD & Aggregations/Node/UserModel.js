//Importing Required Modules
const mongoose=require("mongoose")
const validator=require("validator")        //For Validation which is used in the Schema
const bcrypt=require("bcryptjs")            //For Encrpto=ion the Password and etc...

//Creating A Schema
const User_Info = new mongoose.Schema({
    name:{
        type:String,
        require:[true,'Please Enter Your Name ']
    },
    email:{
        type:String,
        require:[true,"Please Enter Your Email"],
        unique:true,
        lowercase:true,
        validate:[validator.isEmail,"Please Enter Valid Email"]
    },
    photo:String,
    role:{
        type:String,
        enum:['user','admin'],
        default:'user'
    },
    password:{
        type:String,
        require:[true,"Please Enter a Password"],
        minlength:8,
        select:false 
    },
    confirmPassword:{
        type:String,
        require:[true,"Please Enter a Confirm Password"],
        validate:{
            validator:function(val){
               return val===this.password
            },
            message:"Password and Confrm Password does not match!"
        }
    },
    pswdChangedAt:Date
})

//Checking Password Modified or Not And Encrypt Funtion
User_Info.pre("save",async function(next){
    if(!this.isModified("password")) return next();
    this.password = await bcrypt.hash(this.password,12)
    this.confirmPassword=undefined;     //Not Shown the response Page
    next();
})

//Comparing DBpswd and pswd which is user entered
User_Info.methods.comparePasswordDB=async function(pswd,pswdDB){
    return await bcrypt.compare(pswd,pswdDB)
}

//Password Changed or Not If Yes Change That into Timestamp
User_Info.methods.isPasswordChanged=async function(JWTtimestamp){
    if(this.pswdChangedAt){
        const pswdChangedTimestamp=parseInt(this.pswdChangedAt.getTime()/1000,10)
        console.log(pswdChangedTimestamp,JWTtimestamp);
        return JWTtimestamp<pswdChangedTimestamp;
    }
    return false
}

//Creating A Model on the User_Info Schema
const User = mongoose.model("User",User_Info)

//Exporting Module To Use in Aother Files
module.exports=User