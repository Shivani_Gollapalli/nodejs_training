//Importing Required Modules
const express=require('express')
const AuthController=require('./AuthController.js')

//Initiating Router
const Router=express.Router()

//Routeing API's
Router.route('/signup').post(AuthController.signup)
Router.route('/login').post(AuthController.login)

//Exporting Module for use in anothe Files
module.exports=Router;