//Importing Model for Storing the data in the DB
const StdDetails=require('./Model.js')

//GET MTHOD
// exports.getAllStudents=(req,res)=>{
//     res.status(200).json({
//         status:"success",
//         count:students.length,
//         data:{
//             students
//         }
//     })
// }
// exports.singleid=(req,res)=>{
//     const id=req.params.id*1
//     let student=students.find(el=>el.id===id);
//     if(!mocie){
//         return res.status(404).json({
//             status:"fail",
//             message:"Movie with ID " +id+ "is not found"
//         })
//     }
//     res.status(200).json({
//         status:"Success",
//         data:{
//             student
//         }
//     })
// }
//POST METHOD
// exports.createStudent=(req,res)=>{
//     const newId=students[students.length-1].id*1;
//     const newStudent=Object.assign({id:newId},req.body)
//     students.push(newStudent)
//     fs.writeFile('./std.json',JSON.stringify(students),(err)=>{
//         res.status(201).json({
//             status:"success",
//             data:{
//                 newStudent
//             }
//         })
//     })
// }
//PATCH METHOD
// exports.updateStudent=(req,res)=>{
//     let id=req.params.id*1;
//     let studentToUpdate=students.find(el=>el.id===id);
//     if(!studentToUpdate){
//         returnres.status(404).json({
//             status:"Fail",
//             message:"Message"
//         })
//     }
//     let index=students.indexOf(studentToUpdate);
//     Object.assign(studentToUpdate,req.body)
//     students[index]=studentToUpdate;
//     fs.writeFile('./std.json',JSON.stringify(students),(err)=>{
//         res.status(200).json({
//             status:"success",
//             data:{
//                 student:studentToUpdate
//             }
//         })
//     })
// }
//DELETE METHOD
// exports.deleteStudent=(req,res)=>{
//     const id=req.params.id*1;
//     const studentTodelete=students.find(el=>el.id===id);
//     if(!studentTodelete){
//         returnres.status(404).json({
//             status:"Fail",
//             message:"Message"
//         })
//     }
//     const index=students.indexOf(studentTodelete);
//     Object.assign(studentTodelete,req.body)
//     students[index]=studentTodelete;
//     fs.writeFile('./std.json',JSON.stringify(students),(err)=>{
//         res.status(200).json({
//             status:"success",
//             data:null
//         })
//     })
// }



//GET All Student
exports.getAllStudents=async(req,res)=>{
    try{
        const AllStudents=await StdDetails.find()
        res.status(200).json({
            status:"Success",
            Count:AllStudents.length,
            Data:{
                AllStudents
            }
        })
    }catch(err){
        res.status(404).json({
            status:"Fail",
            message:err.message
        })
    }
    
}
//Create A Student
exports.createStudent= async (req,res)=>{
    try{
        const Studentdata = await StdDetails.create(req.body);
        res.status(201).json({
            status:"Success",
            Data:{
                Studentdata
            }
        })
    }catch(err){
        res.status(400).json({
            status:"Fail",
            message:err.message
        })
    }
}

//Delete Student
exports.deleteStudent = (req, res) => {
    try {
      const id = req.params.id * 1;
      const studentToDelete = students.find(el => el.id === id);
  
      if (!studentToDelete) {
        return res.status(404).json({
          status: "Fail",
          message: "Student not found",
        });
      }
  
      const index = students.indexOf(studentToDelete);
      Object.assign(studentToDelete, req.body);
      students[index] = studentToDelete;
  
      fs.writeFile('./std.json', JSON.stringify(students), (err) => {
        res.status(200).json({
          status: "success",
          data: null,
        });
      });
    } catch (error) {
      // Handle any error that occurs within the try block
      res.status(500).json({
        status: "error",
        message: "An error occurred while processing the request.",
      });
    }
  };
  

//Aggregation Funtions
exports.getStdStats=async(req,res)=>{
    try{
        const stats= await StdDetails.aggregate([  //Aggreagation Pipeline based on multistage calculations
            {$match:{age:{$gte:22}}},    //Output of the 1 stage will be giving input to the second stage 
            {$group:{                   //Group stage will be applicabele to the Output of the match stage 
                _id:"$join_year",       //Grouping Based on the Student Join_Year
                avgMarks:{$avg:"$subject_marks"},
                avgAttendence:{$avg:"$attendence"},
                minCGPA:{$min:"$cgpa"},
                maxCGPA:{$max:"$cgpa"},
                marksTotal:{$sum:"$subject_marks"},
                TotalCount:{$sum:1}
            }},
            {$sort:{minCGPA:1}}
        ]);
        res.status(200).json({
            status:"Success",
            Count:stats.length,
            Data:{
                stats,
            }
        })

    }catch(err){
        res.status(404).json({
            status:"Fail",
            message:err.message
        })
    }


}
//Aggregations Unwind and Prject and Limit
exports.getStdByAddress= async (req,res)=>{
    try{
        const address=req.params.address;
        const students=await StdDetails.aggregate([
            {$unwind:"$address"},
            {$group:{
                _id:"$address",
                stdCount:{$sum:1},
                students:{$push:"$full_name"}
            }},
            {$addFields:{Address:"$_id"}},
            {$project:{_id:0}},         // This projection stage is used to reshape the documents in the pipeline by including or excluding specific fields.When _id is set to 0, it means that the field will not be included in the output documents.
            {$sort:{stdCount:-1}},      //The -1 indicates descending order, while 1 would indicate ascending order.
            // {$limit:6}
            {$match:{Address:address}}
        ]);
        res.status(200).json({
            status:"Success",
            Count:students.length,
            Data:{
                students
            }
        })

    }catch(err){
        res.status(404).json({
            status:"Fail",
            message:err.message
        })
    }

}

