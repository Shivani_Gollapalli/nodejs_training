const https = require("node:https")
const maxcalls=12
const start=Date.now()
for(let i=0;i<maxcalls;i++){
    https
    .request("https://www.google.com",(res)=>{
        res.on("data",()=>{})
        res.on("end",()=>{
            console.log(`request : ${i+1}`,Date.now()-start)
        })
    })
    .end()
}