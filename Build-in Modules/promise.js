// const fs=require ("node:fs/promises")
// fs.readFile("greet","utf-8")
// .then((data)=> console.log(data))
// .catch((error)=>console.log(error))

const fs=require ("node:fs/promises")
async function readFile(){
    try{
        const data=await fs.readFile("greet","utf-8")
        console.log(data)
    }catch(error){
        console.log(error)
    }
}

readFile()