// const http = require ("node:http")
// const server = http.createServer((req,res)=>{
//     res.writeHead(200,{"Content-Text":"text/plain"})
//     res.end("Server Runs Suceessfully...!!")
// })
// server.listen(3000,()=>{
//     console.log("Server is Running...!!")
// })

// const http = require ("node:http")
// const server = http.createServer((req,res)=>{
//     const superman = {
//         first:"Shivani",
//         last:"Gollapalli",
//     }
//     res.writeHead(200,{"Content-Text":"application/json"})
//     res.end(JSON.stringify(superman))
//     // res.end("Server is Run Successfully and got disconnected ...!!")
// })
// server.listen(3000,()=>{
//     console.log("Server is Running on port number 3000")
// })

// const http = require ("node:http")
// const fs = require("node:fs")
// const server = http.createServer((req,res)=>{
//     const https= fs.readFileSync("./index.html","utf-8")
//     res.writeHead(200,{"content-type":"text/html"})
//     // res.end(JSON.stringify(superman))
//     // res.end("<h1>Server is Run Successfully and got disconnected ...!!</h1>")
//     res.end(https)
// })
// server.listen(3000,()=>{
//     console.log("Server is Running on port number 3000")
// })


// const http = require ("node:http")
// const fs = require("node:fs")
// const server = http.createServer((req,res)=>{
//     const name ="Shivani Gollapalli"
//     res.writeHead(200,{"content-type":"text/html"})
//     let html= fs.readFileSync("./index.html","utf-8")
//     html=html.replace("{{name}}",name)
//     res.end(html)
// })
// server.listen(3000,()=>{
//     console.log("Server is Running on port number 3000")
// })

const http = require ("node:http")
const server =http.createServer((req,res)=>{
    // res.end(req.url)
    if(req.url==='/'){
        res.writeHead(200,{"Content-Text":"text/plain"})
        res.end("Home Page...!")
    }
    else if(req.url==="/about"){
        res.writeHead(200,{"Content-Text":"text/plain"})
        res.end("About Page...!")
    }
    else if(req.url==="/api"){
        const obj={
            first:"Shivani",
            last:"Gollapalli"
        }
        res.writeHead(200,{"Content-Text":"application/json"})
        res.end(JSON.stringify(obj))
    }
    else{
        res.writeHead(404)
        res.end("Page not Found...!")
    }

})

server.listen(3000,()=>{
    console.log("Server is Running on port number 3000")
})