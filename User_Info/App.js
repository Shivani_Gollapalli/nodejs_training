//Importing Required Modules
const mongoose=require("mongoose")
const dotenv=require("dotenv")
dotenv.config({path:"./config.env"})
const express=require("express");
const productRoute=require("./Routers.js")
let App=express()

//MiddleWare
App.use(express.json());
App.use(morgan('dev'))
App.use((req,res,next)=>{
    req.requestedAt=new Date().toISOString();
    next()
})

App.use('/product',productRoute)
// Get-ExecutionPolicy
// Set-ExecutionPolicy RemoteSigned -Scope Process
// npm install -g nodemon --save 
//nodemon FileName

module.exports=App;