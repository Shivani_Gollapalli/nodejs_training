//Importing Required Modules
const dotenv=require("dotenv")
dotenv.config({path:"./config.env"})
const App=require('./App.js')
const mongoose=require("mongoose")

console.log(process.env)

//Conneting String
mongoose.connect(process.env.REMOTE,{
    useNewUrlParser:true
}).then((conn)=>{
        // console.log(conn)
        console.log("DB Connected Successfully...!")
}).catch((error)=>{
    console.log("Some error has occcured")
})

//Creating Server
const port=3000;
App.listen(port,()=>{
    console.log("Server is Running...!!!")
}) 