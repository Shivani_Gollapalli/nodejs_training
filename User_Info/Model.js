//Importing Required Modules
const mongoose=require("mongoose")
//Craating Scehema
const Product = new mongoose.Schema({
    ProductName:{
        type:String,
        required:[true,"Name is Required Field"],
        unique:true
    },
    Description:String,
    Madein:Number,
    Rating:{
        type:Number,
        default:1.0
    },
    Currency:Number
})

//Creating Model
const ProductDetails = mongoose.model("productDetail",Product)

//Exporting
module.exports=ProductDetails