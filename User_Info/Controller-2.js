//Importing Required Modules
const ProductDetails=require("./Model.js")

//GET_All_USERS
exports.getAllUsers=async(req,res)=>{
    try{
        const Products=await ProductDetails.find()
        res.status(200).json({
            status:"Success",
            Count:Products.length,
            Data:{
                Products
            }
        })
    }catch(err){
        res.status(404).json({
            status:"Fail",
            message:err.message
        })
    }
}

//GET_SINGLE_USER
exports.getUser=async (req,res)=>{
   try{
    const Product=await ProductDetails.findById(req.params.id)
    res.status(200).json({
        status:"Success",
        Data:{
            Product
        }
    })
}catch(err){
    res.status(404).json({
        status:"Fail",
        message:err.message
    })
}
}

//CREATE_USER
exports.createUser= async (req,res)=>{
    // const testProduct=new ProductDetails()
    // testProduct.save()
    try{
        const productdet = await ProductDetails.create(req.body);
        res.status(201).json({
            status:"Success",
            Data:{
                ProductDetails:productdet 
            }
        })
    }catch(err){
        res.status(400).json({
            status:"Fail",
            message:err.message
        })
    }

}

//UPDATE_USER
exports.updateUser=async(req,res)=>{
    try{
        const updatedProduct=await ProductDetails.findByIdAndUpdate(req.params.id,req.body,{new:true,runValidators:true})
        res.status(201).json({
            status:"Success",
            Data:{
                updatedProduct
            }
        })
    }catch(err){
        res.status(400).json({
            status:"Fail",
            message:err.message
        })
    }
}

//DELETE_USER
exports.deleteUser= async(req,res)=>{
    try{
        await ProductDetails.findByIdAndDelete(req.params.id)
        res.status(201).json({
            status:"Success",
            Data:{
                data:null
            }
        })
    }catch(err){
        res.status(400).json({
            Status:"Fail",
            Message:err.Message
        })
    }
}

