//Importing Required Modules
const fs=require('fs')
const Model=require("./Model.js")
let userDetails=JSON.parse(fs.readFileSync("./Products.json"))

//CHECK_ID
exports.checkId=(req,res,next,value)=>{
    console.log()
    let User=userDetails.find(el=>el.id===value * 1)
    if(!User){
        return res.status(404).json({
            Status:"Fail",
            Message:"Product with Id " +value+ "is not found"
        })
    }
    next();
}
//Validation Part
exports.validateBody=(req,res,next)=>{
    if(!req.body.category || !req.body.name){
        return res.status(400).json({
            Status:"Fail",
            Message:"Not a Valid Produuct Data"
        })
    }
    next();
}

//GET_All_USERS
exports.getAllUsers=(req,res)=>{
    res.status(200).json({
        Status:"Request Accepted",
        Count:userDetails.length,
        Data:userDetails
    })
}

//GET_SINGLE_USER
exports.getUser=(req,res)=>{
    // console.log(req.params);
    const id=req.params.id*1;
    let user=userDetails.find(el=>el.id===id);

    // if(!user){
    //     return res.status(404).json({
    //         status:"Fail",
    //         message:"Movie with Id " +id+ "is not found"
    //     })
    // }

    res.status(200).json({
        Status:"Success",

         data:{
            user:user
         }   
        })
    res.send("Test Movie");
}

//CREATE_USER
exports.createUser=(req,res)=>{
    const newId=userDetails[userDetails.length-1].id+1;
    const newUser=Object.assign({id:newId},req.body)
    userDetails.push(newUser)
    fs.writeFile('./Products.json',JSON.stringify(userDetails),()=>{
        res.status(201).json({
            Status:"Request Accepted",
            CreatedUser:newUser
        })
    })
}

//UPDATE_USER
exports.updateUser=(req,res)=>{
    let id=req.params.id*1
    let userToUpdate = userDetails.find(el=>el.id===id)

    // if(!userToUpdate){
    //     return res.status(404).json({
    //         Status:'Fail',
    //         message:"No movie object with ID" +id+ "is found"
    //     })
    // }
    let index=userDetails.indexOf(userToUpdate)
    Object.assign(userToUpdate,req.body)
    userDetails[index]=userToUpdate
    fs.writeFile('./Products.json',JSON.stringify(userDetails),(err)=>{
        res.status(200).json({
            Status:"Success",
            data:{
                UpdatedUser:userToUpdate
            }
        })
    })
}

//DELETE_USER
exports.deleteUser=(req,res)=>{
    const id=req.params.id*1;
    const userToDelete=userDetails.find(el=>el.id===id);

    // if(!userToDelete){
    //     return res.status(404).json({
    //         Status:"Fail",
    //         Message:"No movie object with Id " +id+ "is found to delete"
    //     })
    // }

    const index=userDetails.indexOf(userToDelete)
    userDetails.splice(index,1)
    fs.writeFile('./Products.json',JSON.stringify(userDetails),(err)=>{
        res.status(204).json({
            Status:"success",
            Data:{
                User:null
            }
        })
    })
}

