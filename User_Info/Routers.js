//Importing Modules
const express=require("express");
const productController=require("./Controller-2.js")

//INITATING THE ROUTER
const router=express.Router()
// router.param('id',productController.checkId)

//Routing
router.route('/details')
.get(productController.getAllUsers)
.post(productController.createUser)
router.route('/:id')
.get(productController.getUser)
.patch(productController.updateUser)
.delete(productController.deleteUser)

// //Rounting Using Route
// App.route('/user/details')
// .get(getAllUsers)
// .post(createUser)
// App.route("/user/:id")
// .get(getUser)
// .patch(updateUser)
// .delete(deleteUser)

module.exports=router
