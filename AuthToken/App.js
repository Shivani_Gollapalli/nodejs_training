const express=require("express")
const cors=require('cors')
const authRouter=require('./AuthRouter.js')

let App=express()

App.use(express.json())
App.use(cors());

App.use('/users',authRouter)

module.exports=App

// Get-ExecutionPolicy
// Set-ExecutionPolicy RemoteSigned -Scope Process
// npm install -g nodemon --save 
//nodemon FileName
