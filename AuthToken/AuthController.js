const User=require('./Model.js')
const jwt=require('jsonwebtoken')

const signToken=id=>{
    return jwt.sign({id},process.env.SECRET_STR,{
        expiresIn:process.env.LOGIN_EXP
    }
)}
exports.signup = async(req,res,next)=>{
    try{
        const newUser=await User.create(req.body);
        // const token=signToken(newUser._id);
        res.status(201).json({
            status:"Success",
            // Token:token,
            data:{
                user:newUser
            }
        })
    }catch(err){
        res.status(404).json({
            status:"Fail",
            message:err.message
        })
    }
}

exports.login=async (req,res,next)=>{
    try{
        const email=await req.body.email;
        const password=await req.body.password;
        if (!email || !password) {
            return res.status(400).json({
                status: "Fail",
                message: "Please provide an email and password for login",
            });
        }

        //Check if user Exists with Given Email
        const user = await User.findOne({email}).select('+password');
        const isMatch = await user.comparePasswordDB(password,user.password);

        if(!user || !isMatch){
            return res.status(400).json({
                status: "Fail",
                message: "Incorrect Email or Password",
            });
        }
        const token=signToken(user._id)


        res.status(200).json({
            status:"Success",
            token,
            user
        })
    }catch(err){
        res.status(404).json({
            status:"Fail",
            message:err.message
        })
    }
}
