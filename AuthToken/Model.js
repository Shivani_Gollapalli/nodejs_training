const mongoose=require("mongoose")
const validator=require("validator")
const bcrypt=require("bcryptjs")

const User_Info = new mongoose.Schema({
    name:{
        type:String,
        require:[true,'Please Enter Your Name ']
    },
    email:{
        type:String,
        require:[true,"Please Enter Your Email"],
        unique:true,
        lowercase:true,
        validate:[validator.isEmail,"Please Enter Valid Email"]
    },
    photo:String,
    password:{
        type:String,
        require:[true,"Please Enter a Password"],
        minlength:8,
        select:false 
    },
    confirmPassword:{
        type:String,
        require:[true,"Please Enter a Confirm Password"],
        validate:{
            validator:function(val){
               return val===this.password
            },
            message:"Password and Confrm Password does not match!"
        }
    }
})

User_Info.pre("save",async function(next){
    if(!this.isModified("password")) return next();
    this.password = await bcrypt.hash(this.password,12)
    this.confirmPassword=undefined;
    next();
})

User_Info.methods.comparePasswordDB=async function(pswd,pswdDB){
    return await bcrypt.compare(pswd,pswdDB)
}
const User = mongoose.model("User",User_Info)

module.exports=User