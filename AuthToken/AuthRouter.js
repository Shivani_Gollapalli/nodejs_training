const express=require('express')
const AuthController=require('./AuthController.js')

const Router=express.Router()

Router.route('/signup').post(AuthController.signup)
Router.route('/login').post(AuthController.login)

module.exports=Router;