const mongoose=require("mongoose")

//Craating Scehema
const USER_INFO = new mongoose.Schema({
    first_name:{
        type:String
    },
    last_name:{
        type:String
       
    },
    email:{
        type:String
    },
    phone_number:{
        type:Number
    }
})

//Creating Model
const UserDetails = mongoose.model("UserDetails",USER_INFO)

//Exporting
module.exports=UserDetails  