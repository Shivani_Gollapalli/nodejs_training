const express=require("express");
const userRoute=require("./Routers.js")
const cors=require("cors")
let App=express()

//MiddleWare
App.use(cors())
App.use(express.json());
App.use((req,res,next)=>{
    req.requestedAt=new Date().toISOString();
    next()
})

//Routing 
App.use('/user',userRoute)

// Get-ExecutionPolicy
// Set-ExecutionPolicy RemoteSigned -Scope Process
// npm install -g nodemon --save 
//nodemon FileName

module.exports=App;