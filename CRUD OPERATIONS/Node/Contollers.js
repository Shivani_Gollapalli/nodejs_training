//Importing Required Functions
const UserDetails=require("./Model.js")

//AllUSers
exports.getAllUsers=async(req,res)=>{
    try{
        const AllUsers=await UserDetails.find()
        res.status(200).json({
            status:"Success",
            Count:AllUsers.length,
            Data:{
                AllUsers
            }
        })
    }catch(err){
        res.status(404).json({
            status:"Fail",
            message:err.message
        })
    }
    
}

//Single User
exports.getUser=async (req,res)=>{
   try{
    const User=await UserDetails.findById(req.params.id)
    res.status(200).json({
        status:"Success",
        Data:{
            User
        }
    })
}catch(err){
    res.status(404).json({
        status:"Fail",
        message:err.message
    })
}
}

//Create User
exports.createUser= async (req,res)=>{
    try{
        const Userdata = await UserDetails.create(req.body);
        res.status(201).json({
            status:"Success",
            Data:{
                Userdata
            }
        })
    }catch(err){
        res.status(400).json({
            status:"Fail",
            message:err.message
        })
    }

}

//Update User
exports.updateUser=async(req,res)=>{
    try{
        let id  = req.body._id;
        delete req.body._id;
        console.log("test",id,req.body);
        const updatedUser=await UserDetails.findByIdAndUpdate(id,req.body,{new:true,runValidators:true})
        res.status(201).json({
            status:"Success",
            Data:{
                updatedUser
            }
        })
    }catch(err){
        res.status(400).json({
            status:"Fail",
            message:err.message
        })
    }
}

//Delete User
exports.deleteUser= async(req,res)=>{
    try{
        console.log("req.params",req.params)
        await UserDetails.findByIdAndDelete(req.params.id)
        //await UserDetails.deleteOne({_id : req.params.id})
        res.status(204).json({
            status:"Success",
            Data:{
                data:null
            }
        })
    }catch(err){
        res.status(404).json({
            Status:"Fail",
            message:err.message
        })
    }
}

