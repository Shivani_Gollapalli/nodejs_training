//Importing Modules
const express=require("express");
const userController=require("./Contollers.js")
const router=express.Router()

//Routing
router.route('/details')
.get(userController.getAllUsers)
router.route('/create')
.post(userController.createUser)
router.route('/:id')
.get(userController.getUser)
router.route("/update")
.patch(userController.updateUser)
router.route("/delete/:id")
.delete(userController.deleteUser)

// //Rounting Using Route
// App.route('/user/details')
// .get(getAllUsers)
// .post(createUser)
// App.route("/user/:id")
// .get(getUser)
// .patch(updateUser)
// .delete(deleteUser)

module.exports=router
