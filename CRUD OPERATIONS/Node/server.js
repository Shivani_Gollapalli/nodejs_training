//Importing Required Modules
const dotenv=require("dotenv")
dotenv.config({path:"./config.env"})
const App=require('./App.js')
const mongoose=require("mongoose")
const cors=require("cors")

//Conneting String
mongoose.connect(process.env.LOCAL,{
    useNewUrlParser:true
}).then((conn)=>{
    // console.log(conn)
    console.log("DB Connected Successfully...!")
}).catch((error)=>{
    console.log("Some error has occcured")
})

//Creating Server
const port=3000;
App.listen(port,()=>{
    console.log("Server is Running...!!!")
}) 