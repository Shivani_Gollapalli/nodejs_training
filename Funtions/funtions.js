// function sayHello(){
//     console.log(`Hello ${this.name}`)
// }

// const person = {name:"Shivani"}

// sayHello.call(person)


// function introduce(language1, language2) {
//     console.log(`I speak ${language1} and ${language2}.`);
//   }
// const person = { name: 'Alice' };
// const languages = ['English', 'Spanish'];
// introduce.apply(person, languages); // Output: I speak English and Spanish.

  

function greet(greeting) {
    console.log(`${greeting}, ${this.name}!`);
  }
  
const person = { name: 'Mary' };
  
const greetPerson = greet.bind(person, 'Good morning');
greetPerson(); // Output: Good morning, Mary!
  