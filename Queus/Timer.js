// setTimeout(()=>console.log("Settimeout 1 "),0);
// setTimeout(()=>{
//     console.log("Settimeout 2 ");
//     process.nextTick(()=>
//         console.log("This is the inner next tick inside settimeout 2")
//     )
// },0)
// setTimeout(()=>console.log("Settimeout 3 "),0);

// process.nextTick(() => console.log("this is process.nextTick 1"))
// process.nextTick(() => {
//     console.log("this is process.nextTick 2");
//     process.nextTick(() => 
//         console.log("this is the inner next tick inside next tick") 
// ); 
// });
// process.nextTick(() => console.log("this is process.nextTick 3"));

// Promise.resolve().then(() => console.log("this is Promise.resolve 1"));
// Promise.resolve().then(() => {
//     console.log("this is Promise.resolve 2");
//     process.nextTick(() =>
//         console.log("this is the inner next tick inside Promise then block")
// );
// })
// Promise.resolve().then(() => console.log("this is Promise.resolve 3"));

setTimeout(()=>console.log("SetTime-1"),1000)
setTimeout(()=>console.log("SetTime-2"),1000)
setTimeout(()=>console.log("SetTime-3"),1000)